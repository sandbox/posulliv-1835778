<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Schema
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Database\SchemaObjectExistsException;
use Drupal\Core\Database\SchemaObjectDoesNotExistException;
use Drupal\Core\Database\Schema as DatabaseSchema;

use Exception;

/**
 * @addtogroup schemaapi
 * @{
 */

class Schema extends DatabaseSchema {

  protected function createTableSql($name, $table) {
    $sql_fields = array();
    foreach ($table['fields'] as $field_name => $field) {
      $sql_fields[] = $this->createFieldSql($field_name, $this->processField($field));
    }

    $sql_keys = array();
    if (isset($table['primary key']) && is_array($table['primary key'])) {
      $sql_keys[] = 'PRIMARY KEY (' . implode(', ', $table['primary key']) . ')';
    }
    if (isset($table['unique keys']) && is_array($table['unique keys'])) {
      foreach ($table['unique keys'] as $key_name => $key) {
        $sql_keys[] = 'CONSTRAINT ' . $this->prefixNonTable($name, $key_name, 'key') . ' UNIQUE (' . implode(', ', $key) . ')'; 
      }
    }

    $sql = "CREATE TABLE {" . $name . "} (\n\t";
    $sql .= implode(",\n\t", $sql_fields);
    if (count($sql_keys) > 0) {
      $sql .= ",\n\t";
    }
    $sql .= implode(",\n\t", $sql_keys);
    $sql .= "\n)";
    $statements[] = $sql;

    if (isset($table['indexes']) && is_array($table['indexes'])) {
      foreach ($table['indexes'] as $key_name => $key) {
        $statements[] = $this->_createIndexSql($name, $key_name, $key);
      }
    }

    // TODO: table and column comments

    return $statements;
  }

  protected function createFieldSql($name, $spec) {
    $sql = '"' . $name . '" ' . $spec['akiban_type'];

    if (isset($spec['type']) && $spec['type'] == 'serial') {
      unset($spec['not null']);
    }

    if (in_array($spec['akiban_type'], array('varchar', 'character', 'blob')) && isset($spec['length'])) {
      $sql .= '(' . $spec['length'] . ')';
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $sql .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }

    if (! empty($spec['unsigned'])) {
      // TODO: when we support CHECK constraints, should add that here
    }

    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $sql .= ' NOT NULL';
      }
      else {
        $sql .= ' NULL';
      }
    }
    if (isset($spec['default'])) {
      $default = is_string($spec['default']) ? "'" . $spec['default'] . "'" : $spec['default'];
      $sql .= " default $default";
    }

    return $sql;
  }

  protected function processField($field) {
    if (! isset($field['size'])) {
      $field['size'] = 'normal';
    }

    // Set the correct database-engine specific datatype.
    // In case one is already provided, force it to lowercase.
    if (isset($field['akiban_type'])) {
      $field['akiban_type'] = drupal_strtolower($field['akiban_type']);
    }
    else {
      $map = $this->getFieldTypeMap();
      $field['akiban_type'] = $map[$field['type'] . ':' . $field['size']];
    }

    if (! empty($field['unsigned'])) {
      /*
       * unsigned datatypes are not support in Akiban. MySQL apparently
       * uses unsigned integers to ensure only positive numbers are 
       * inserted. For Akiban, we will will just use a larger int
       * datatype.
       */
      if (! isset($map)) {
        $map = $this->getFieldTypeMap();
      }
      switch ($field['akiban_type']) {
        case 'smallint':
          $field['akiban_type'] = $map['int:medium'];
          break;
        case 'int':
          $field['akiban_type'] = $map['int:big'];
          break;
      }
    }

    if (isset($field['type']) && $field['type'] === 'serial') {
      unset($field['not null']);
    }

    return $field;
  }

  /*
   * We over-ride this method in Akiban because we do not have a table_catalog
   * column in our information_schema.tables table.
   */
  protected function buildTableNameCondition($table_name, $operator = '=', $add_prefix = TRUE) {
    $info = $this->connection->getConnectionOptions();

    // Retrive the table name and schema
    $table_info = $this->getPrefixInfo($table_name, $add_prefix);

    $condition = new Condition('AND');
    $condition->condition('table_schema', $table_info['schema']);
    $condition->condition('table_name', $table_info['table'], $operator);
    return $condition;
  }

  public function getFieldTypeMap() {
    static $map = array(
      'varchar:normal' => 'varchar',
      'char:normal' => 'character',

      'text:tiny' => 'blob',
      'text:small' => 'blob',
      'text:medium' => 'blob',
      'text:big' => 'blob',
      'text:normal' => 'blob',

      'int:tiny' => 'smallint',
      'int:small' => 'smallint',
      'int:medium' => 'int',
      'int:big' => 'bigint',
      'int:normal' => 'int',

      'float:tiny' => 'real',
      'float:small' => 'real',
      'float:medium' => 'real',
      'float:big' => 'double precision',
      'float:normal' => 'real',

      'numeric:normal' => 'numeric',

      'blob:big' => 'blob',
      'blob:normal' => 'blob',

      'serial:tiny' => 'serial',
      'serial:small' => 'serial',
      'serial:medium' => 'serial',
      'serial:big' => 'serial',
      'serial:normal' => 'serial',
      );
    return $map;
  }

  public function renameTable($table, $new_name) {
    if (! $this->tableExists($table)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot rename @table to @table_new: table @table doesn't exist.",
                                                  array('@table' => $table, '@table_new' => $new_name)));
    }
    if ($this->tableExists($new_name)) {
      throw new SchemaObjectExistsException(t("Cannot rename @table to @table_new: table @table_new already exists.",
                                            array('@table' => $table, '@table_new' => $new_name)));
    }

    $prefixInfo = $this->getPrefixInfo($new_name);
    $this->connection->query('RENAME TABLE {' . $table . '} TO ' . $prefixInfo['table']);
  }

  public function dropTable($table) {
    if (! $this->tableExists($table)) {
      return FALSE;
    }

    $this->connection->query('DROP TABLE {' . $table . '}');
    return TRUE;
  }

  public function addField($table, $field, $spec, $new_keys = array()) {
    if (! $this->tableExists($table)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot add field @table.@field: table doesn't exist.",
                                                  array('@field' => $field, '@table' => $table)));
    }
    if ($this->fieldExists($table, $field)) {
      throw new SchemaObjectExistsException(t("Cannot add field @table.@field: field already exists.",
                                            array('@field' => $field, '@table' => $table)));
    }

    $query = 'ALTER TABLE {' . $table . '} ADD COLUMN ';
    $query .= $this->createFieldSql($field, $this->processField($spec));
    $this->connection->query($query);

    // TODO: column comments
  }

  public function dropField($table, $field) {
    if (! $this->fieldExists($table, $field)) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP COLUMN "' . $field . '"');
    return TRUE;
  }

  public function fieldSetDefault($table, $field, $default) {
    if (! $this->fieldExists($table, $field)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot set default value of field @table.@field: field doesn't exist.",
                                                  array('@table' => $table, '@field' => $field)));
    }

    if (! isset($default)) {
      $default = 'NULL';
    }
    else {
      $default = is_string($default) ? "'$default'" : $default;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER COLUMN "' . $field . '" SET DEFAULT ' . $default);
  }

  public function fieldSetNoDefault($table, $field) {
    if (! $this->fieldExists($table, $field)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot remove default value of field @table.@field: field doesn't exist.",
                                                  array('@table' => $table, '@field' => $field)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER COLUMN "' . $field . '" DROP DEFAULT');
  }

  public function indexExists($table, $name) {
    $query = "SELECT 1 FROM information_schema.indexes WHERE table_name = '$table' AND index_name = '$name'";
    return (bool) $this->connection->query($query)->fetchField();
  }

  public function addPrimaryKey($table, $fields) {
    if (! $this->tableExists($table)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot add primary key to table @table: table doesn't exist.",
                                                  array('@table' => $table)));
    }
    if ($this->constraintExists($table, 'pkey')) {
      throw new SchemaObjectExistsException(t("Cannot add primary key to table @table: primary key already exists.",
                                            array('@table' => $table)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ADD PRIMARY KEY (' . implode(',', $fields) . ')');
  }

  public function dropPrimaryKey($table) {
    if (! $this->indexExists($table, 'PRIMARY')) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP PRIMARY KEY');
    return TRUE;
  }

  function addUniqueKey($table, $name, $fields) {
    if (! $this->tableExists($table)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot add unique key @name to table @table: table doesn't exist.",
                                                  array('@table' => $table, '@name' => $name)));
    }
    if ($this->indexExists($table, $name . '_key')) {
      throw new SchemaObjectExistsException(t("Cannot add unique key @name to table @table: unique key already exists.",
                                            array('@table' => $table, '@name' => $name)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ADD CONSTRAINT "' . $this->prefixNonTable($table, $name, 'key') . '" UNIQUE (' . implode(',', $fields) . ')');
  }

  public function dropUniqueKey($table, $name) {
    if (! $this->indexExists($table, $name . '_key')) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT "' . $this->prefixNonTable($table, $name, 'key') . '"');
    return TRUE;
  }

  public function addIndex($table, $name, $fields) {
    if (! $this->tableExists($table)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot add index @name to table @table: table doesn't exist.",
                                                  array('@table' => $table, '@name' => $name)));
    }
    if ($this->indexExists($table, $name)) {
      throw new SchemaObjectExistsException(t("Cannot add index @name to table @table: index already exists.",
                                            array('@table' => $table, '@name' => $name)));
    }

    $this->connection->query($this->_createIndexSql($table, $name, $fields));
  }

  public function dropIndex($table, $name) {
    if (! $this->indexExists($table, $name)) {
      return FALSE;
    }

    $this->connection->query('DROP INDEX ' . $this->prefixNonTable($table, $name, 'idx'));
    return TRUE;
  }

  public function changeField($table, $field, $field_new, $spec, $new_keys = array()) {
  }

  protected function _createIndexSql($table, $name, $fields) {
    $query = 'CREATE INDEX "' . $this->prefixNonTable($table, $name, 'idx') . '" ON {' . $table . '} (';
    $query .= $this->_createKeySql($fields) . ')';
    return $query;
  }

  protected function _createKeys($table, $new_keys) {
    if (isset($new_keys['primary key'])) {
      $this->addPrimaryKey($table, $new_keys['primary key']);
    }
    if (isset($new_keys['unique keys'])) {
      foreach ($new_keys['unique keys'] as $name => $fields) {
        $this->addUniqueKey($table, $name, $fields);
      }
    }
    if (isset($new_keys['indexes'])) {
      foreach ($new_keys['indexes'] as $name => $fields) {
        $this->addIndex($table, $name, $fields);
      }
    }
  }

  protected function _createKeySql($fields) {
    $return = array();
    foreach ($fields as $field) {
      if (is_array($field)) {
        // TODO: Akiban does not currently support function-based or prefix indexes
        //$return[] = 'substr(' . $field[0] . ', 1, ' . $field[1] . ')';
        $return[] = '"' . $field[0] . '"';
      }
      else {
        $return[] = '"' . $field . '"';
      }
    }
    return implode(', ', $return);
  }

}

/**
 * @} End of "addtogroup schemaapi".
 */
