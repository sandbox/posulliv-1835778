<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\pgsql\Install\Tasks
 */

namespace Drupal\Core\Database\Driver\akiban\Install;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Install\Tasks as InstallTasks;

use Exception;

/**
 * Akiban specific install functions.
 */
class Tasks extends InstallTasks {

  // Akiban uses the PostgreSQL PDO driver.
  protected $pdoDriver = 'pgsql';

  public function name() {
    return st('Akiban');
  }

  public function minimumVersion() {
    return '1.4.2';
  }

}
