<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Insert
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Query\Insert as QueryInsert;

/**
 * @ingroup database
 * @{
 */

class Insert extends QueryInsert {

  public function execute() {
    // If validation fails, simply return NULL. Note that validation routines
    // in preExecute() may throw exceptions instead.
    if (!$this->preExecute()) {
      return NULL;
    }

    // get sequence name for retrieving last_insert_id
    $sequenceName = $this->connection->retrieveSequenceName($this->table);
    if (! empty($sequenceName)) {
      $this->queryOptions['sequence_name'] = $sequenceName;
    }

    // If we're selecting from a SelectQuery, finish building the query and
    // pass it back, as any remaining options are irrelevant.
    if (!empty($this->fromQuery)) {
      $sql = (string) $this;
      // The SelectQuery may contain arguments, load and pass them through.
      return $this->connection->query($sql, $this->fromQuery->getArguments(), $this->queryOptions);
    }   

    $last_insert_id = 0;

    // Each insert happens in its own query in the degenerate case. However,
    // we wrap it in a transaction so that it is atomic where possible. On many
    // databases, such as SQLite, this is also a notable performance boost.
    $transaction = $this->connection->startTransaction();

    try {
      $sql = (string) $this;
      foreach ($this->insertValues as $insert_values) {
        $last_insert_id = $this->connection->query($sql, $insert_values, $this->queryOptions);
      }   
    }   
    catch (Exception $e) {
      // One of the INSERTs failed, rollback the whole batch.
      $transaction->rollback();
      // Rethrow the exception for the calling code.
      throw $e; 
    }   

    // Re-initialize the values array so that we can re-use this query.
    $this->insertValues = array();

    // Transaction commits here where $transaction looses scope.

    return $last_insert_id;
  }

  public function __toString() {
    static $AKIBAN_KEYWORDS = array(
      'session'   => true,
      'external'  => true,
      'translate' => true,
      'function'  => true,
    );
    // Create a sanitized comment string to prepend to the query.
    $comments = $this->connection->makeComment($this->comments);

    // quote the insert fields. many drupal columns are reserved keywords
    // also skip default fields. Leaving them out of INSERT list will
    // ensure they are assigned their default values
    // TODO: figure out a cleaner way to deal with this
    $i = 0;
    $defaultFieldsSize = count($this->defaultFields);
    foreach ($this->insertFields as $key => $value) {
      if ($i >= $defaultFieldsSize) {
        // if an akiban keyword, quote it
        if (isset($AKIBAN_KEYWORDS[$value])) {
          $this->insertFields[$key] = '"' . $value . '"';
        } else {
          $this->insertFields[$key] = $value;
        }
      }
      $i++;
    }

    if (!empty($this->fromQuery)) {
      return $comments . 'INSERT INTO {' . $this->table . '} (' . implode(', ', $this->insertFields) . ') ' . $this->fromQuery;
    }

    // For simplicity, we will use the $placeholders array to inject
    // default keywords even though they are not, strictly speaking,
    // placeholders for prepared statements.
    $placeholders = array();
    $placeholders = array_pad($placeholders, count($this->insertFields), '?');

    return $comments . 'INSERT INTO {' . $this->table . '} (' . implode(', ', $this->insertFields) . ') VALUES (' . implode(', ', $placeholders) . ')';
  }

}

/**
 * @} End of "ingroup database".
 */
