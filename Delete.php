<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Delete
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Query\Delete as QueryDelete;

/**
 * @ingroup database
 * @{
 */

class Delete extends QueryDelete {
  // TODO
}

/**
 * @} End of "ingroup database".
 */
