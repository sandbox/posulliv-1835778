<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Truncate
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Query\Truncate as QueryTruncate;

/**
 * @ingroup database
 * @{
 */

class Truncate extends QueryTruncate {

  public function __toString() {
    return 'DELETE FROM {' . $this->connection->escapeTable($this->table) . '} ';
  }

}

/**
 * @} End of "ingroup database".
 */
