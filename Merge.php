<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Merge
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Query\Merge as QueryMerge;

/**
 * @ingroup database
 * @{
 */

class Merge extends QueryMerge {
  // TODO
}

/**
 * @} End of "ingroup database".
 */
