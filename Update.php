<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Update
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Query\Update as QueryUpdate;

use PDO;

/**
 * @ingroup database
 * @{
 */

class Update extends QueryUpdate {

  /**
   * Implements PHP magic __toString method to convert the query to a string.
   *
   * @return string
   *   The prepared statement.
   */
  public function __toString() {
    static $AKIBAN_KEYWORDS = array(
      'session'   => true,
      'external'  => true,
      'translate' => true,
      'function'  => true,
    );
    // Create a sanitized comment string to prepend to the query.
    $comments = $this->connection->makeComment($this->comments);

    // Expressions take priority over literal fields, so we process those first
    // and remove any literal fields that conflict.
    $fields = $this->fields;
    $update_fields = array();
    foreach ($this->expressionFields as $field => $data) {
      if (isset($AKIBAN_KEYWORDS[$field])) {
        $update_fields[] = '"' . $field . '"' . '=' . $data['expression'];
      } else {
        $update_fields[] = $field . '=' . $data['expression'];
      }
      unset($fields[$field]);
    }

    $max_placeholder = 0;
    foreach ($fields as $field => $value) {
      // if an akiban keyword, quote it
      if (isset($AKIBAN_KEYWORDS[$field])) {
        $update_fields[] = '"' . $field . '"' . '=:db_update_placeholder_' . ($max_placeholder++);
      } else {
        $update_fields[] = $field . '=:db_update_placeholder_' . ($max_placeholder++);
      }
    }

    $query = $comments . 'UPDATE {' . $this->connection->escapeTable($this->table) . '} SET ' . implode(', ', $update_fields);

    if (count($this->condition)) {
      $this->condition->compile($this->connection, $this);
      // There is an implicit string cast on $this->condition.
      $query .= "\nWHERE " . $this->condition;
    }

    return $query;
  }

}

/**
 * @} End of "ingroup database".
 */
