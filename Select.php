<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Select
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Query\Select as QuerySelect;

/**
 * @ingroup database
 * @{
 */

class Select extends QuerySelect { 

  public function forUpdate($set = TRUE) {
    // TODO: Akiban does not currently support FOR UPDATE
    return $this;
  }

}

/**
 * @} End of "ingroup database".
 */
