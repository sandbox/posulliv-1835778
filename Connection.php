<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Connection
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Connection as DatabaseConnection;
use Drupal\Core\Database\StatementInterface;

use PDO;
use PDOException;

/**
 * @addtogroup database
 * @{
 */

class Connection extends DatabaseConnection {

  /**
   * Whether or not the active transaction (if any) will be rolled back.
   *
   * @var boolean
   */
  protected $willRollback;

  public function __construct(array $connection_options = array()) {
    // default to transaction support, except if explicitly passed FALSE.
    $this->transactionSupport = !isset($connection_options['transactions']) || ($connection_options['transactions'] !== FALSE);

    // default to TCP connection on port 15432
    if (empty($connection_options['port'])) {
      $connection_options['port'] = 15432;
    }

    $this->connectionOptions = $connection_options;

    $dsn = 'pgsql:host=' . $connection_options['host'] . ' dbname=' . $connection_options['database'] . ' port=' . $connection_options['port'];

    // Allow PDO options to be overridden.
    $connection_options += array(
      'pdo' => array(),
    );

    parent::__construct($dsn,
                        $connection_options['username'],
                        $connection_options['password'], 
                        $connection_options['pdo']);
  }

  public function driver() {
    return 'akiban';
  }

  public function databaseType() {
    return 'akiban';
  }

  public function escapeField($field) {
    // TODO: cleaner way to do this?
    $validFieldName = preg_replace('/[^A-Za-z0-9_.]+/', '', $field);
    $identifiers = explode(".", $validFieldName);
    if (count($identifiers) == 1) {
      return '"' . $identifiers[0] . '"';
    }
    return $identifiers[0] . '."' . $identifiers[1] . '"';
  }

  public function escapeAlias($field) {
    $validFieldName = preg_replace('/[^A-Za-z0-9_.]+/', '', $field);
    return '"' . $validFieldName . '"';
  }

  /**
   * Overrides \Drupal\Core\Database\Connection::createDatabase().
   *
   * @param string $database
   *   The name of the database to create.
   *
   * @throws DatabaseNotFoundException
   */
  public function createDatabase($database) {
    // Escape the database name.
    $database = Database::getConnection()->escapeDatabase($database);

    try {
      // Create the database and set it as active.
      $this->exec("CREATE SCHEMA $database");
      $this->exec("SET SCHEMA $database");
    }
    catch (\Exception $e) {
      throw new DatabaseNotFoundException($e->getMessage());
    }
  }

  public function mapConditionOperator($operator) {
    // We don't want to override any of the defaults.
    static $specials = array(
      'LIKE' => array('postfix' => " ESCAPE 'x'"),
      'NOT LIKE' => array('postfix' => " ESCAPE 'x'"),
    );
    return isset($specials[$operator]) ? $specials[$operator] : NULL;
  }

  public function makeSequenceName($table, $field) {
    $sql = "SELECT sequence_name FROM information_schema.columns WHERE table_name = '" . $table . "' AND column_name = '" . $field . "'";
    return $this->query($sql)->fetchField();
  }

  public function nextId($existing_id = 0) {
    $sequenceName = $this->makeSequenceName('sequences', 'value');
    $id = $this->query("SELECT nextval('" . $this->connectionOptions['database'] . "', '" . $sequenceName . "')")->fetchField();
    // TODO: deal with case of $existing_id > $id
    return $id;
  }

  /*
   * Retrieve the sequence name for the serial column of the
   * given table.
   */
  public function retrieveSequenceName($tableName) {
    // first get the serial column in this table
    $sql = "SELECT sequence_name FROM information_schema.columns WHERE table_name = '" . $tableName . "' AND sequence_name IS NOT NULL";
    return $this->query($sql)->fetchField();
  }

  /*
   * Akiban needs to override this PDO function because our syntax for retrieving
   * the last inserted value of a sequence is different to PostgreSQL.
   */
  public function akibanLastInsertId($sequenceName) {
    $schemaName = $this->connectionOptions['database'];
    $sql = "SELECT CURRVAL('" . $schemaName . "', '" . $sequenceName . "')";
    return $this->query($sql)->fetchField();
  }

  public function query($query, array $args = array(), $options = array()) {
    // Use default values if not already set.
    $options += $this->defaultOptions();

    // The PDO PostgreSQL driver has a bug which
    // doesn't type cast booleans correctly when
    // parameters are bound using associative
    // arrays.
    // See http://bugs.php.net/bug.php?id=48383
    foreach ($args as &$value) {
      if (is_bool($value)) {
        $value = (int) $value;
      }
    }

     try {
      // We allow either a pre-bound statement object or a literal string.
      // In either case, we want to end up with an executed statement object,
      // which we pass to PDOStatement::execute.
      if ($query instanceof DatabaseStatementInterface) {
        $stmt = $query;
        $stmt->execute(NULL, $options);
      }
      else {
        $this->expandArguments($query, $args);
        $stmt = $this->prepareQuery($query);
        $stmt->execute($args, $options);
      }

      switch ($options['return']) {
        case Database::RETURN_STATEMENT:
          return $stmt;
        case Database::RETURN_AFFECTED:
          return $stmt->rowCount();
        case Database::RETURN_INSERT_ID:
          /*
           * get sequence name, if none then return
           * same as Database::RETURN_NULL
           */
          if (isset($options['sequence_name'])) {
            return $this->akibanLastInsertId($options['sequence_name']);
          }
          return 0; // is this correct?
        case Database::RETURN_NULL:
          return;
        default:
          throw new PDOException('Invalid return directive: ' . $options['return']);
      }
    }
    catch (PDOException $e) {
      if ($options['throw_exception']) {
        // Add additional debug information.
        if ($query instanceof StatementInterface) {
          $e->query_string = $stmt->getQueryString();
        }
        else {
          $e->query_string = $query;
        }
        $e->args = $args;
        throw $e; 
      }
      return NULL;
    }
  }

  public function queryRange($query, $from, $count, array $args = array(), array $options = array()) {
    return $this->query($query . ' LIMIT ' . (int) $count . ' OFFSET ' . (int) $from, $args, $options);
  }

  public function queryTemporary($query, array $args = array(), array $options = array()) {
    $tableName = $this->generateTemporaryTableName();
    // TODO: Akiban does not currently support creating in-memory temporary tables
    $this->query(preg_replace('/^SELECT/i', 'CREATE TABLE {' . $tablename . '} AS SELECT', $query), $args, $options);
  }

  public function rollback($savepoint_name = 'drupal_transaction') {
    if (! $this->inTransaction()) {
      throw new TransactionNoActiveException();
    }

    if (! in_array($savepoint_name, $this->transactionLayers)) {
      return;
    }

     // We need to find the point we're rolling back to, all other savepoints
    // before are no longer needed.
    while ($savepoint = array_pop($this->transactionLayers)) {
      if ($savepoint == $savepoint_name) {
        // Mark whole stack of transactions as needed roll back.
        $this->willRollback = TRUE;
        // If it is the last the transaction in the stack, then it is not a
        // savepoint, it is the transaction itself so we will need to roll back
        // the transaction rather than a savepoint.
        if (empty($this->transactionLayers)) {
          break;
        }
        return;
      }
    }
    if ($this->supportsTransactions()) {
      PDO::rollBack();
    }
  }

  public function pushTransaction($name) {
    if (! $this->supportsTransactions()) {
      return;
    }
    if (isset($this->transactionLayers[$name])) {
      throw new TransactionNameNonUniqueException($name . " is already in use.");
    }
    if (! $this->inTransaction()) {
      PDO::beginTransaction();
    }
    $this->transactionLayers[$name] = $name;
  }

  public function popTransaction($name) {
    if (! $this->supportsTransactions()) {
      return;
    }
    if (! $this->inTransaction()) {
      throw new TransactionNoActiveException();
    }

    // Commit everything since SAVEPOINT $name.
    while($savepoint = array_pop($this->transactionLayers)) {
      if ($savepoint != $name) continue;

      // If there are no more layers left then we should commit or rollback.
      if (empty($this->transactionLayers)) {
        // If there was any rollback() we should roll back whole transaction.
        if ($this->willRollback) {
          $this->willRollback = FALSE;
          PDO::rollBack();
        }
        elseif (! PDO::commit()) {
          throw new TransactionCommitFailedException();
        }
      }
      else {
        break;
      }
    }
  }

}

/**
 * @} End of "addtogroup database".
 */
