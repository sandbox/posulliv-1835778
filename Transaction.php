<?php

/**
 * @file
 * Definition of Drupal\Core\Database\Driver\akiban\Transaction
 */

namespace Drupal\Core\Database\Driver\akiban;

use Drupal\Core\Database\Transaction as QueryTransaction;

/**
 * @ingroup database
 * @{
 */

class Transaction extends QueryTransaction { }

/**
 * @} End of "ingroup database".
 */
